import collections
import queue

import simplejson as json

import slanglib

LEFT_CENTERED = "left"
RIGHT_CENTERED = "right"

BREAKER_PATTERN = [(["VB", "IN"], LEFT_CENTERED), (["NNS", "VBG"], LEFT_CENTERED), (["IN"], LEFT_CENTERED),
                   ([","], LEFT_CENTERED), (["POS"], RIGHT_CENTERED), (["CC"], LEFT_CENTERED)]
IGNORE = ["DT"]

DIRECT_SPEC = "spec"
CORE = "core"
VERSION = " --- Descriptor Parser v. 18.11.22.1 ---"

print (VERSION)


def parse_all(file_in, file_out):
    # file_in = "/Users/juliano/PycharmProjects/new_selected_classes"
    # file_out = "./data/classes.json"

    descriptors = []
    i = 0
    with open(file_in, "r") as f:
        for line in f:
            # line = unicode(line, 'utf-8')[:-1]
            parts = line.split("  -:-  ")

            class_name = slanglib.process_camelcase(parts[0]).lower()
            uri = parts[1].strip()

            core, tags = _parse(class_name)

            desc = Descriptor(class_name, uri, core)
            descriptors.append(desc)

    print (len(descriptors))

    if len(descriptors) > 0:
        with open(file_out, "w") as fout:
            json.dump({"data": descriptors}, fout, separators=(',', ': '), sort_keys=True)


def _parse(descriptor):
    #adjust
    descriptor = descriptor.replace(") ", " ")
    descriptor = descriptor.replace(" (", " ")

    if descriptor.startswith("a 1 "):
        descriptor = descriptor.replace("a 1 ", "a1 ")

    tokens, tags = slanglib.process_tokens_and_tags(descriptor)
    slanglib.adjust_pos_tags(tokens, tags)

    chunk = get_chunks(tokens, tags, 0, len(tokens) - 1)
    return chunk, tags


def find_breaker_pattern(tags, start, end):
    if start < end:
        for pattern, centre in BREAKER_PATTERN:
            if len(pattern) == 2:
                indexes = [i for i, tag in enumerate(tags[start:end + 1]) if
                           tag.startswith(pattern[0]) and i + 1 < len(tags[start:end + 1]) and tags[
                               start + i + 1].startswith(pattern[1])]
            else:
                indexes = [i for i, tag in enumerate(tags[start:end + 1]) if tag.startswith(pattern[0])]

            if len(indexes) > 0:
                matched = indexes[0]
                if tags[matched + start] == "NNS":
                    matched += 1

                return matched + start, matched + start + len(pattern), centre

    return None, None, None


def get_chunks(tokens, tags, start, end):
    left, right, centre = find_breaker_pattern(tags, start, end)

    if left:
        lchunk = get_chunks(tokens, tags, start, left - 1)
        rchunk = get_chunks(tokens, tags, right, end)

        relation = " ".join(tokens[left:right])
        relation += "-" + tags[left]

        if centre == LEFT_CENTERED:
            lchunk.specs.append(Relation(relation, rchunk))
            local_core = lchunk
        else:
            rchunk.specs.append(Relation(relation, lchunk))
            local_core = rchunk

    else:

        effective_term = tokens[end]
        tag = tags[end]
        if end != start and tags[end - 1] in IGNORE:
            pure_term = " ".join(tokens[end - 1:end + 1])
            end -= 1
        else:
            pure_term = tokens[end]

        local_core = Chunk(pure_term, effective_term, tag, [])

        i = end - 1
        while i >= start:
            effective_term = tokens[i]
            tag = tags[i]

            if i != start and tags[i - 1] in IGNORE:
                pure_term = " ".join(tokens[i - 1:i + 1])
                i -= 1
            else:
                pure_term = tokens[i]

            spec = Chunk(pure_term, effective_term, tag, [])

            local_core.specs.append(Relation(DIRECT_SPEC, spec))
            i -= 1

    return local_core

# Data Types
Relation = collections.namedtuple('Relation', 'type content')
Chunk = collections.namedtuple('Chunk', 'pure_term effective_term tag specs')
Descriptor = collections.namedtuple('Descriptor', 'name uri core')


def main():
    clazz = 'air carriers discontinued in 1993'
    #clazz = slanglib.process_camelcase(clazz).lower()
    print (clazz)
    descriptor = _parse(clazz)
    print (clazz)
    print ('core: ', descriptor[0].pure_term)
    q = queue.Queue()

    for spec in descriptor[0].specs:
        q.put(spec)

    while not q.empty():
        spec = q.get()
        print (spec.type, '|', spec.content.pure_term)
        for sub in spec.content.specs:
            q.put(sub)


if __name__ == "__main__":
    main()