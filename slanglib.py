import re
import nltk

def process_camelcase(s):
    words = re.sub(r"\r", "", s)
    words = re.sub(r"\n", "", words)
    words = re.sub(r"([a-z]+)('?)([A-Z]+)", r"\1\2 \3", words)
    words = re.sub(r"([A-Z]+)('?)([A-Z][a-z]+)", r"\1\2 \3", words)
    words = re.sub(r"([a-zA-Z])([0-9]+)", r"\1 \2", words)
    words = re.sub(r"([0-9]+)([A-Z])", r"\1 \2", words)
    words = words.replace(",", ", ")
    words = words.replace("*", " ")
    words = words.replace("(", " (")
    words = words.replace(")", ") ")

    words = re.sub(r"(UK)([A-Z])", r"\1 \2", words)
    words = words.replace("D Js", "DJs")

    words = re.sub(r"(\.)(\w{2,})", r"\1 \2", words)


    return words.strip()

def process_tokens_and_tags(sentence):
    tokens = nltk.tokenize.word_tokenize(sentence)
    tagged_tokens = nltk.pos_tag(tokens)
    tags = [tag for token, tag in tagged_tokens]

    return tokens, tags

def adjust_pos_tags(tokens, tags):
    # TO ^(VB) -> IN ^(VB)
    indexes = [i for i, tag in enumerate(tags) if tag == "TO" and i+1 < len(tags) and tags[i+1] != "VB"]
    for index in indexes:
        tags[index] = "IN"

    indexes = [i for i, tag in enumerate(tags) if tag == "NNPS"]
    for index in indexes:
        tags[index] = "NNS"

    indexes = [i for i, tag in enumerate(tags) if tag == "MD" and i+1 < len(tags) and tags[i+1].startswith("VB")]
    for index in indexes:
        tokens[index+1] = " ".join(tokens[index:index+2])
        tags[index + 1] += "/MD"
        del tokens[index:index+1]
        del tags[index:index+1]

    indexes = [i for i, tag in enumerate(tags) if tag.startswith("W") and i+1 < len(tokens) and tokens[i+1] in ("is", "are", "was", "were")]
    for index in indexes:
        tokens[index] = " ".join(tokens[index:index+2])
        del tokens[index+1:index+2]
        del tags[index+1:index+2]

    indexes = [i for i, tag in enumerate(tags) if tag.startswith("W") and i+1 < len(tags) and tags[i+1].startswith("VB")]
    for index in indexes:
        tokens[index+1] = " ".join(tokens[index:index+2])
        tags[index + 1] += "/W"
        del tokens[index:index+1]
        del tags[index:index+1]

    # A&M I = NNP
    indexes = [i for i, token in enumerate(tokens) if token == "a" and i+2 < len(tokens) and tokens[i+1] == "&" and tokens[i+2] == "m"]
    for index in indexes:
        del tokens[index:index+3]
        tokens.insert(index, "a&m")
        del tags[index:index+3]
        tags.insert(index, "NNP")

    indexes = [i for i, token in enumerate(tokens) if token in ["hurling", "wrestling", "clothing"]]
    for index in indexes:
        tags[index] = "NN"

    indexes = [i for i, token in enumerate(tokens) if token == "governing" and i+1 < len(tokens) and tokens[i+1] == "bodies"]
    for index in indexes:
        tags[index] = "JJ"

    indexes = [i for i, token in enumerate(tokens) if token == "manufacturing" and i+1 < len(tokens) and tokens[i+1] == "companies"]
    for index in indexes:
        tags[index] = "JJ"

    indexes = [i for i, token in enumerate(tokens) if token == "training" and i+1 < len(tokens) and tokens[i+1] == "venues"]
    for index in indexes:
        tags[index] = "JJ"

    indexes = [i for i, token in enumerate(tokens) if token == "telecasting" and i+1 < len(tokens) and tokens[i+1] == "companies"]
    for index in indexes:
        tags[index] = "JJ"

    indexes = [i for i, token in enumerate(tokens) if token == "racing" and i+1 < len(tokens) and tokens[i+1] in ["car", "cars"]]
    for index in indexes:
        tags[index] = "JJ"

    indexes = [i for i, token in enumerate(tokens) if token in ["modeling", "programming"] and i+1 < len(tokens) and tokens[i+1] in["language", "languages"]]
    for index in indexes:
        tags[index] = "JJ"

    indexes = [i for i, token in enumerate(tokens) if token == "banking" and i+1 < len(tokens) and tokens[i+1] == "case"]
    for index in indexes:
        tags[index] = "JJ"

    indexes = [i for i, token in enumerate(tokens) if token == "hong" and i+1 < len(tokens) and tokens[i+1] == "kong"]
    for index in indexes:
        tags[index] = "NNP"
        tags[index+1] = "NNP"


def fix_pos_tags(tokens, tags):
    
    try:
        # (video|ballet|opera|operas|spirits|fiends) = NNP
        indexes = [i for i, token in enumerate(tokens) if token in ["video", "ballet", "operas", "spirits", "fiends"] and tags[i].startswith("VB")]
        for index in indexes:
            tags[index] = "NN"

        indexes = [i for i, tag in enumerate(tags) if tag == "MD" and i+1 < len(tags) and tags[i+1].startwith("VB")]
        for index in indexes:
            tokens[index+1] = " ".join(tokens[index:index+2])
            del tokens[index:index+1]
            del tags[index:index+1]

        # Talent: VBD -> NN
        indexes = [i for i, token in enumerate(tokens) if token == "talent"]
        for index in indexes:
            tags[index] = "NN"

        # State: VBP -> JJ
        indexes = [i for i, token in enumerate(tokens) if token == "state"]
        for index in indexes:
            tags[index] = "JJ"

        # Pittsburgh: IN -> NNP
        indexes = [i for i, token in enumerate(tokens) if token == "pittsburgh"]
        for index in indexes:
            tags[index] = "NNP"
        
        # Steroid: VBD -> NN
        indexes = [i for i, token in enumerate(tokens) if token == "steroid"]
        for index in indexes:
            tags[index] = "NN"
        
        # Whyte: NN -> NNP
        indexes = [i for i, token in enumerate(tokens) if token == "whyte"]
        for index in indexes:
            tags[index] = "NNP"
        
        # Michaels: NNS -> NNP
        indexes = [i for i, token in enumerate(tokens) if token == "michaels"]
        for index in indexes:
            tags[index] = "NNP"
        
        # Paralympic: VBD -> JJ
        indexes = [i for i, token in enumerate(tokens) if token == "paralympic"]
        for index in indexes:
            tags[index] = "JJ"
        
        # Hurling: VBG -> NN
        indexes = [i for i, token in enumerate(tokens) if token == "hurling"]
        for index in indexes:
            tags[index] = "NN"
        
        # Naval: VBP -> JJ
        indexes = [i for i, token in enumerate(tokens) if token == "naval"]
        for index in indexes:
            tags[index] = "JJ"
        
        # African: VBD -> JJ
        indexes = [i for i, token in enumerate(tokens) if token == "african"]
        for index in indexes:
            tags[index] = "JJ"
        
        # Orange County,
        indexes = [i for i, token in enumerate(tokens) if token == "orange" and tokens[i+1] == "county,"]
        for index in indexes:
            tags[index] = "NNP"
            tags[index+1] = "NNP"
        
        # New York -> NNP NNP
        indexes = [i for i, token in enumerate(tokens) if token == "new" and tokens[i+1] == "york"]
        for index in indexes:
            tags[index] = "NNP"
            tags[index+1] = "NNP"
        
        # Emmy: VBP -> NNP
        indexes = [i for i, token in enumerate(tokens) if token == "emmy"]
        for index in indexes:
            tags[index] = "NNP"
        
        # Clothing: VBG -> NN
        indexes = [i for i, token in enumerate(tokens) if token == "clothing"]
        for index in indexes:
            tags[index] = "NN"
        
        # Ramazzotti: VBP -> NNP
        indexes = [i for i, token in enumerate(tokens) if token == "ramazzotti"]
        for index in indexes:
            tags[index] = "NNP"
        
        # Jesus Christ -> NNP NNP
        indexes = [i for i, token in enumerate(tokens) if token == "jesus" and tokens[i+1] == "christ"]
        for index in indexes:
            tags[index] = "NNP"
            tags[index+1] = "NNP"

        # Yago Identifier -> NNP NNP
        indexes = [i for i, token in enumerate(tokens) if token == "yago" and tokens[i+1] == "identifier"]
        for index in indexes:
            tags[index] = "NNP"
            tags[index+1] = "NNP"

    except Exception:
        print("Error on fix_postags:\n", tokens, tags)
